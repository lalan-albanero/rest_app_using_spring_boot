package com.restapp.rest_app_demo.service;

import org.springframework.stereotype.Service;

import com.restapp.rest_app_demo.model.Responsemodel;

@Service
public class StringService {
    public Responsemodel countVowelsAndSplcharacter(String str) {
        str = str.toLowerCase();
        int vowelCount = 0;
        int specialCount = 0;
        String vowels = "aeiou";
        String special = "~!@#$%^&*";
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            int index = vowels.indexOf(ch);
            int index1 = special.indexOf(ch);

            if (index >= 0) {

                vowelCount += 1;
            }
            if (index1 >= 0) {

                specialCount += 1;
            }

        }
        return new Responsemodel(vowelCount, specialCount);
    }
}
