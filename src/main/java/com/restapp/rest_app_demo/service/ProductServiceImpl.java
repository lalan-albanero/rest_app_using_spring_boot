package com.restapp.rest_app_demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.restapp.rest_app_demo.model.Product;
import com.restapp.rest_app_demo.repo.StudentRepo;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private StudentRepo studentRepo;

    @Override
    public List<Product> getProducts() {
        // TODO Auto-generated method stub
        return this.studentRepo.findAll();
    }

    @Override
    public Product addProduct(Product p) {
        // TODO Auto-generated method stub
        Product save = this.studentRepo.save(p);
        return save;
    }

    @Override
    public Product updateProduct(Integer id, Product p) {
        // TODO Auto-generated method stub
        p.setId(id);
        return studentRepo.save(p);

    }

    @Override
    public String deleteProduct(Integer id) {
        // TODO Auto-generated method stub
        studentRepo.deleteById(id);
        return "Product deleted successfully..";
    }

}
