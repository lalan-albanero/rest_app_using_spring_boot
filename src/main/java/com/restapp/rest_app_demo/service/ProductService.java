package com.restapp.rest_app_demo.service;

import java.util.List;

import com.restapp.rest_app_demo.model.Product;

public interface ProductService {
    List<Product> getProducts();

    Product addProduct(Product p);

    Product updateProduct(Integer id, Product p);

    String deleteProduct(Integer id);
}
