package com.restapp.rest_app_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class RestAppDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestAppDemoApplication.class, args);
	}

}
