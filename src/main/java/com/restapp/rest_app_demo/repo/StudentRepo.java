package com.restapp.rest_app_demo.repo;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.restapp.rest_app_demo.model.Product;

public interface StudentRepo extends MongoRepository<Product,Integer>{
    
}
