package com.restapp.rest_app_demo.model;

public class Responsemodel {
    int vowelCount;
    int specialCharacterCount;
    
    public Responsemodel(int vowelCount, int specialCharacterCount) {
        this.vowelCount = vowelCount;
        this.specialCharacterCount = specialCharacterCount;
    }
    public int getVowelCount() {
        return vowelCount;
    }
    public void setVowelCount(int vowelCount) {
        this.vowelCount = vowelCount;
    }
    public int getSpecialCharacterCount() {
        return specialCharacterCount;
    }
    public void setSpecialCharacterCount(int specialCharacterCount) {
        this.specialCharacterCount = specialCharacterCount;
    }
    
}
