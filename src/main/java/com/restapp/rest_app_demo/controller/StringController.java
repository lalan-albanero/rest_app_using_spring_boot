package com.restapp.rest_app_demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restapp.rest_app_demo.service.StringService;

@RestController
public class StringController {
    @Autowired
    StringService ss;

    @GetMapping("/getString")
    ResponseEntity<?> countVowel(@RequestBody String str) {

        // return ResponseEntity.ok(r1);
        return ResponseEntity.ok(ss.countVowelsAndSplcharacter(str));
    }

    @GetMapping("/")
    String help() {
        return "good to go..";
    }

}
