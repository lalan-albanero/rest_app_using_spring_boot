package com.restapp.rest_app_demo.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restapp.rest_app_demo.model.Product;
import com.restapp.rest_app_demo.repo.StudentRepo;
import com.restapp.rest_app_demo.service.ProductService;

@RestController
public class ProductServiceController {
    // private static Map<Integer, Product> productrepo = new HashMap<>();
    // static {

    // Product p1 = new Product();
    // p1.setId(1);
    // p1.setName("honey");
    // Product p2 = new Product();
    // p2.setId(2);
    // p2.setName("laptop");
    // productrepo.put(p1.getId(), p1);
    // productrepo.put(p2.getId(), p2);
    // }
    @Autowired
    ProductService ps;

    @GetMapping(value = "/products")
    public ResponseEntity<?> getProducts() {
        return ResponseEntity.ok(this.ps.getProducts());
    }

    @PostMapping("/addProduct")
    public ResponseEntity<?> addProduct(@RequestBody Product p) {
        // productrepo.put(p.getId(), p);
        return ResponseEntity.ok(ps.addProduct(p));
    }

    @PutMapping("/updateProduct/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable("id") Integer id,
            @RequestBody Product p) {
        // productrepo.remove(id);
        // productrepo.put(id, p);
        return ResponseEntity.ok(ps.updateProduct(id, p));
    }

    @DeleteMapping("/deleteProduct/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Integer id) {
        // productrepo.remove(id);
        return ResponseEntity.ok(ps.deleteProduct(id));
    }
}
